package com.lrodriguez.movieexample.Services;

import com.lrodriguez.movieexample.Models.SchedulesResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**
 * Created by lrodriguez on 3/27/2018.
 */

public interface CinemaServices {

    @GET("/v2/schedules")
    Call<SchedulesResponse> getSchedules(
            @Header("api_key") String apiKey,
            @Query("country_code") String countryCode,
            @Query("cities") String city,
            @Query("include_cinemas") boolean includeCinemas,
            @Query("include_movies") boolean includeMovies
    );
}
