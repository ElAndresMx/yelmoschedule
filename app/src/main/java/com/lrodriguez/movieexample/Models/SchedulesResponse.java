package com.lrodriguez.movieexample.Models;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

/**
 * Created by lrodriguez on 3/27/2018.
 */

public class SchedulesResponse {

    private List<Cinema> cinemas;
    private List<Date> dates;
    private List<Format> formats;
    private List<Movie> movies;
    private List<Route> routes;
    private List<Schedule> schedules;



    public class Cinema{
        private String address;
        @SerializedName("city_id")
        private int cityId;
        private int id;
        private String lat;
        private String lng;
        private String name;
        private String phone;
        private String position;
        private Setting settings;
        @SerializedName("vista_id")
        private int vistaId;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public int getCityId() {
            return cityId;
        }

        public void setCityId(int cityId) {
            this.cityId = cityId;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }

        public Setting getSettings() {
            return settings;
        }

        public void setSettings(Setting settings) {
            this.settings = settings;
        }

        public int getVistaId() {
            return vistaId;
        }

        public void setVistaId(int vistaId) {
            this.vistaId = vistaId;
        }

        public class Setting{
            @SerializedName("is_special_prices")
            private boolean isSpecialPrices;
            @SerializedName("type_food_sales")
            private String typeFoodSales;

            public boolean isSpecialPrices() {
                return isSpecialPrices;
            }

            public void setSpecialPrices(boolean specialPrices) {
                isSpecialPrices = specialPrices;
            }

            public String getTypeFoodSales() {
                return typeFoodSales;
            }

            public void setTypeFoodSales(String typeFoodSales) {
                this.typeFoodSales = typeFoodSales;
            }
        }
    }
    public class Format{
        @SerializedName("display_name")
        private String displayName;
        private String icon;
        private int id;
        private String name;

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
    public class Movie{
        private List<String> categories;
        private String code;
        private String genre;
        private int id;
        private int length;
        private List<Media> media;
        private String name;
        private String position;
        private String rating;
        private String synopsis;


        public List<String> getCategories() {
            return categories;
        }

        public void setCategories(List<String> categories) {
            this.categories = categories;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getGenre() {
            return genre;
        }

        public void setGenre(String genre) {
            this.genre = genre;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getLength() {
            return length;
        }

        public void setLength(int length) {
            this.length = length;
        }

        public List<Media> getMedia() {
            return media;
        }

        public void setMedia(List<Media> media) {
            this.media = media;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public String getSynopsis() {
            return synopsis;
        }

        public void setSynopsis(String synopsis) {
            this.synopsis = synopsis;
        }

        public class Media{
            private String code;
            private String resource;
            private String type;

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getResource() {
                return resource;
            }

            public void setResource(String resource) {
                this.resource = resource;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }
        }
    }
    public class Route{
        private String code;
        private Size sizes;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public Size getSizes() {
            return sizes;
        }

        public void setSizes(Size sizes) {
            this.sizes = sizes;
        }

        public class Size{
            private String large;
            private String medium;
            private String small;
            @SerializedName("x-large")
            private String xLarge;

            public String getLarge() {
                return large;
            }

            public void setLarge(String large) {
                this.large = large;
            }

            public String getMedium() {
                return medium;
            }

            public void setMedium(String medium) {
                this.medium = medium;
            }

            public String getSmall() {
                return small;
            }

            public void setSmall(String small) {
                this.small = small;
            }

            public String getxLarge() {
                return xLarge;
            }

            public void setxLarge(String xLarge) {
                this.xLarge = xLarge;
            }
        }
    }
    public class Schedule{
        @SerializedName("cinema_id")
        private int cinemaId;
        @SerializedName("city_id")
        private int cityId;
        private List<ScheduleDate> dates;
        @SerializedName("is_special_prices")
        private boolean isSpecialPrices;
        @SerializedName("movie_id")
        private int movieId;

        public int getCinemaId() {
            return cinemaId;
        }

        public void setCinemaId(int cinemaId) {
            this.cinemaId = cinemaId;
        }

        public int getCityId() {
            return cityId;
        }

        public void setCityId(int cityId) {
            this.cityId = cityId;
        }

        public List<ScheduleDate> getDates() {
            return dates;
        }

        public void setDates(List<ScheduleDate> dates) {
            this.dates = dates;
        }

        public boolean isSpecialPrices() {
            return isSpecialPrices;
        }

        public void setSpecialPrices(boolean specialPrices) {
            isSpecialPrices = specialPrices;
        }

        public int getMovieId() {
            return movieId;
        }

        public void setMovieId(int movieId) {
            this.movieId = movieId;
        }

        public class ScheduleDate{
            private Date date;
            private List<ScheduleFormat> formats;

            public Date getDate() {
                return date;
            }

            public void setDate(Date date) {
                this.date = date;
            }

            public List<ScheduleFormat> getFormats() {
                return formats;
            }

            public void setFormats(List<ScheduleFormat> formats) {
                this.formats = formats;
            }

            public class ScheduleFormat{
                @SerializedName("format_id")
                private int formatId;
                private String language;
                private List<ScheduleShowTime> showtimes;
                @SerializedName("vista_id")
                private String vista_id;

                public int getFormatId() {
                    return formatId;
                }

                public void setFormatId(int formatId) {
                    this.formatId = formatId;
                }

                public String getLanguage() {
                    return language;
                }

                public void setLanguage(String language) {
                    this.language = language;
                }

                public List<ScheduleShowTime> getShowTimes() {
                    return showtimes;
                }

                public void setShowTimes(List<ScheduleShowTime> showTimes) {
                    this.showtimes = showTimes;
                }

                public String getVista_id() {
                    return vista_id;
                }

                public void setVista_id(String vista_id) {
                    this.vista_id = vista_id;
                }

                public class ScheduleShowTime{
                    private String datetime;
                    @SerializedName("early_morning")
                    private boolean earlyMorning;
                    private String screen;
                    private Cinema.Setting settings;
                    @SerializedName("vista_id")
                    private int vistaId;

                    public String getDatetime() {
                        return datetime;
                    }

                    public void setDatetime(String datetime) {
                        this.datetime = datetime;
                    }

                    public boolean isEarlyMorning() {
                        return earlyMorning;
                    }

                    public void setEarlyMorning(boolean earlyMorning) {
                        this.earlyMorning = earlyMorning;
                    }

                    public String getScreen() {
                        return screen;
                    }

                    public void setScreen(String screen) {
                        this.screen = screen;
                    }

                    public Cinema.Setting getSettings() {
                        return settings;
                    }

                    public void setSettings(Cinema.Setting settings) {
                        this.settings = settings;
                    }

                    public int getVistaId() {
                        return vistaId;
                    }

                    public void setVistaId(int vistaId) {
                        this.vistaId = vistaId;
                    }
                }
            }
        }
    }


    public List<Cinema> getCinemas() {
        return cinemas;
    }

    public void setCinemas(List<Cinema> cinemas) {
        this.cinemas = cinemas;
    }

    public List<Date> getDates() {
        return dates;
    }

    public void setDates(List<Date> dates) {
        this.dates = dates;
    }

    public List<Format> getFormats() {
        return formats;
    }

    public void setFormats(List<Format> formats) {
        this.formats = formats;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    public List<Schedule> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<Schedule> schedules) {
        this.schedules = schedules;
    }
}
