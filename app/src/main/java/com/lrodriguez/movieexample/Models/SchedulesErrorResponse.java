package com.lrodriguez.movieexample.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lrodriguez on 3/27/2018.
 */

public class SchedulesErrorResponse {
    private String message;
    @SerializedName("status_code")
    private String statusCode;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
}
