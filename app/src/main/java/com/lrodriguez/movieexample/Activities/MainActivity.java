package com.lrodriguez.movieexample.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lrodriguez.movieexample.Adapters.ShowTimeAdapter;
import com.lrodriguez.movieexample.Models.SchedulesErrorResponse;
import com.lrodriguez.movieexample.Models.SchedulesResponse;
import com.lrodriguez.movieexample.R;
import com.lrodriguez.movieexample.Services.CinemaServices;
import com.squareup.picasso.Picasso;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    Button btnSearchSchedule;
    EditText etCityId;
    ImageView ivMovieImage;
    TextView tvSchedulesLabel;
    TextView tvMovieName;
    TextView tvMovieSynopsis;
    RecyclerView rvSchedules;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initControls();
        initListeners();
    }

    private void initControls(){
        btnSearchSchedule = (Button) findViewById(R.id.btnSearcheSchedule);
        etCityId = (EditText) findViewById(R.id.etCityId);
        ivMovieImage = (ImageView) findViewById(R.id.ivMovieImage);
        tvMovieName = (TextView) findViewById(R.id.tvMovieName);
        tvMovieSynopsis = (TextView) findViewById(R.id.tvMovieSynopsis);
        tvSchedulesLabel = (TextView) findViewById(R.id.tvSchedulesLabel);
        rvSchedules = (RecyclerView) findViewById(R.id.rvSchedulesList);
        rvSchedules.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false));
        rvSchedules.hasFixedSize();
    }

    private void initListeners(){
        btnSearchSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cityCode = etCityId.getText().toString();
                if(!cityCode.isEmpty()) callRetorfitSchedulesServie(cityCode);
                else makeToast("El campo no puede ir vacio");

                v.clearFocus();
            }
        });
    }

    private void callRetorfitSchedulesServie(String cityCode){


        String ENDPOINT = getString(R.string.endpoint);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(ENDPOINT).addConverterFactory(GsonConverterFactory.create()).build();



        CinemaServices cinemaServices = retrofit.create(CinemaServices.class);
        String apikey = getString(R.string.api_key);
        String countryCode = getString(R.string.country_code);

        cinemaServices.getSchedules(apikey,countryCode,cityCode,true,true).enqueue(new Callback<SchedulesResponse>() {
            @Override
            public void onResponse(Call<SchedulesResponse> call, Response<SchedulesResponse> response) {
                Gson gson = new GsonBuilder().create();
                if(response.isSuccessful()){
                    SchedulesResponse schedulesResponse = response.body();
                    Log.e("Respuesta Retrofit | ", response.body().toString());
                    SchedulesResponse.Movie movie = schedulesResponse.getMovies().get(13);

                    tvMovieName.setText(movie.getName());
                    tvMovieSynopsis.setText(movie.getSynopsis());
                    String posterUrl = schedulesResponse.getRoutes().get(1).getSizes().getxLarge() + movie.getMedia().get(0).getResource();

                    Picasso.get()
                            .load(posterUrl)
                            .error(R.drawable.not_found)
                            .into(ivMovieImage);

                    Date currentDate = Calendar.getInstance().getTime();


                    List<SchedulesResponse.Schedule.ScheduleDate.ScheduleFormat.ScheduleShowTime> list = getShowTimeList(movie, currentDate, schedulesResponse.getSchedules());

                    if(list.isEmpty()){
                        tvSchedulesLabel.setText("No hay funciones para hoy :(");
                    }else{
                        ShowTimeAdapter adapter = new ShowTimeAdapter(list);

                        rvSchedules.setAdapter(adapter);

                        adapter.notifyDataSetChanged();
                    }




                }else{
                    SchedulesErrorResponse schedulesErrorResponse;
                    try {
                        schedulesErrorResponse = gson.fromJson(response.errorBody().string(), SchedulesErrorResponse.class);
                        makeToast(schedulesErrorResponse.getStatusCode() + " - " + schedulesErrorResponse.getMessage());
                    } catch (IOException e) {
                        e.printStackTrace();
                        makeToast("Petición no lograda.");
                    }

                }
            }

            @Override
            public void onFailure(Call<SchedulesResponse> call, Throwable t) {
                Log.e("ERROR RETROFIT -> " , t.getLocalizedMessage());
                makeToast("CHECA EL LOG");
            }
        });

    }

    private void makeToast(String msg){
        Toast.makeText(MainActivity.this, msg,Toast.LENGTH_LONG).show();
    }

    private List<SchedulesResponse.Schedule.ScheduleDate.ScheduleFormat.ScheduleShowTime> getShowTimeList(SchedulesResponse.Movie movie, Date currDate , List<SchedulesResponse.Schedule> scheduleList){

        List<SchedulesResponse.Schedule.ScheduleDate.ScheduleFormat.ScheduleShowTime> scheduleShowTimesList = new ArrayList<>();
        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd");

        for(SchedulesResponse.Schedule schedule : scheduleList){
            if(schedule.getMovieId() == movie.getId()){
                Log.d("Pelicula ->", movie.getId() + "");
                for(SchedulesResponse.Schedule.ScheduleDate date : schedule.getDates()) {
                    Log.d("Fecha Actual -> ", parser.format(currDate));
                    Log.d("Fecha a Comparar -> ", parser.format(date.getDate()));
                    if (parser.format(currDate).equals(parser.format(date.getDate()))){
                        Log.d("Fecha ->", date.getDate().toString());
                        List<SchedulesResponse.Schedule.ScheduleDate.ScheduleFormat> formats = date.getFormats();
                        Log.d("Formatos ->", date.getFormats().size() + "");
                        for(SchedulesResponse.Schedule.ScheduleDate.ScheduleFormat format : formats){
                            Log.d("Funciones ->", format.getShowTimes().size() + "");
                            scheduleShowTimesList.addAll(format.getShowTimes());
                        }
                    }
                }
            }
        }

        return scheduleShowTimesList;
    }
}
