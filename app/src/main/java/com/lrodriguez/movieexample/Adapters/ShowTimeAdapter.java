package com.lrodriguez.movieexample.Adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lrodriguez.movieexample.Models.SchedulesResponse;
import com.lrodriguez.movieexample.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by lrodriguez on 3/27/2018.
 */

public class ShowTimeAdapter extends RecyclerView.Adapter<ShowTimeAdapter.ViewHolder>{

    private List<SchedulesResponse.Schedule.ScheduleDate.ScheduleFormat.ScheduleShowTime> mList;

    public ShowTimeAdapter(List<SchedulesResponse.Schedule.ScheduleDate.ScheduleFormat.ScheduleShowTime> mList) {
        this.mList = mList;
    }

    @Override
    public ShowTimeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.schedule_list_item, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ShowTimeAdapter.ViewHolder holder, int position) {
        SchedulesResponse.Schedule.ScheduleDate.ScheduleFormat.ScheduleShowTime showTime = mList.get(position);

        String date = "";
        String time = "";
        try {
            date = getDateFromDateTime(showTime.getDatetime());
            time = getTimeFromDateTime(showTime.getDatetime());

        } catch (ParseException e) {
            e.printStackTrace();
        }


        holder.tvScheduleDate.setText(date);
        holder.tvScheduleTime.setText(time);
        holder.tvScheduleRoom.setText(showTime.getScreen());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvScheduleDate;
        TextView tvScheduleTime;
        TextView tvScheduleRoom;

        public ViewHolder(View view) {
            super(view);

            tvScheduleDate = (TextView) view.findViewById(R.id.tvScheduleDate);
            tvScheduleTime = (TextView) view.findViewById(R.id.tvScheduleTime);
            tvScheduleRoom = (TextView) view.findViewById(R.id.tvScheduleRoom);
        }

    }

    private String getDateFromDateTime(String datetime) throws ParseException {
        SimpleDateFormat parse = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date(String.valueOf(parse.parse(datetime)));

        return formatter.format(date);
    }

    private String getTimeFromDateTime(String datetime) throws ParseException {
        SimpleDateFormat parse = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        SimpleDateFormat formatter = new SimpleDateFormat("hh:mm a");
        Date date = new Date(String.valueOf(parse.parse(datetime)));
        return formatter.format(date);
    }
}
